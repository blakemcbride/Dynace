# Dynace Object-Oriented Extension to C


Dynace is a portable, open-source extension to the C language that adds full object-oriented
capabilities, threads, garbaage collector, and a GUI development system.

Home for the code is at:  [https://github.com/blakemcbride/Dynace.git](https://github.com/blakemcbride/Dynace.git)


